These are scripts intended to be run on the Drupal.org database. They will only
work if:

a) You can ssh <username>@util.drupal.org

b) You have a file called 'pw.php' in your home directory that contains:

<?php
// TODO: Fill in real production values here. :P
$hostname = '';
$username = '';
$password = '';
$database = '';
?>

Scripts are as follows:

- dries-stats.php: Counts number of nodes, users, comments, commits, and
  committers per month for a given year range (_count_stats(2011, 2013);)

