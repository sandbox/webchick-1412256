<?php

require_once $_SERVER['HOME'] . '/pw.php';

$connection = mysql_connect($hostname, $username, $password);
if (!$connection) {
  print "failed to open a connection\n";
}
mysql_select_db($database, $connection);

function _count_stats($start_year, $end_year) {
  print "date\t#nodes\tusers\tcomments\tcommits\tcommitters\n";
  for ($year = $start_year; $year <= $end_year; $year++) {
    for ($month = 1; $month <= 12; $month++) {
      $start = mktime(0, 0, 0, $month, 1, $year);
      if ($month != 12) {
        $end = mktime(0, 0, 0, $month + 1, 1, $year);
      }
      else {
        $end = mktime(0, 0, 0, 1, 1, $year + 1);
      }
      $nodes = mysql_result(mysql_query("SELECT COUNT(*) FROM node n WHERE n.status = 1 AND (n.created >= $start AND n.created < $end)"), 0);
      $users = mysql_result(mysql_query("SELECT COUNT(*) FROM users u WHERE u.status = 1 AND created >= $start AND created < $end"), 0);
      $comments = mysql_result(mysql_query("SELECT COUNT(*) FROM comment c WHERE c.status = 1 AND created >= $start AND created < $end"), 0);
      $commits = mysql_result(mysql_query("SELECT COUNT(DISTINCT(revision)) FROM versioncontrol_operations WHERE author_date >= $start AND author_date < $end AND author <> 'tggm@no-reply.drupal.org'"), 0);
      $committers = mysql_result(mysql_query("SELECT COUNT(DISTINCT(author_uid)) FROM versioncontrol_operations WHERE author_date >= $start AND author_date < $end"), 0);
      print "$month/$year\t$nodes\t$users\t$comments\t$commits\t$committers\n";
    }
  }
}

_count_stats(2001, 2014);

