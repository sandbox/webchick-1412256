<?php

// First, you need a CSV file containing profile data for the mentors field.
// I get this out of Sequel Pro (File > Export > Custom Query Result).
//
// Query to generate this list:
/*
SELECT u.uid, u.name, pv.value
FROM profile_values pv
  INNER JOIN users u ON u.uid = pv.uid
WHERE u.status = 1
  AND pv.fid = 108
  AND value <> ''
ORDER BY u.uid
*/
// Then, I run this script to generate a GML (Graph Modeling Language) file:
// php mentor-graph.php > mentors.gml
// Finally, open it up with and open it up with Gephi: https://gephi.org/
// There is a nice tutorial at https://gephi.org/users/quick-start/

$csv_file = 'mentors.csv';


define('MENTOR_SCORE', 5);

$handle = fopen($csv_file, 'r');
$nodes = array();
$edges = array();

while (($data = fgetcsv($handle)) !== FALSE) {
  // Skip the header row.
  if (!is_numeric($data[0])) {
    continue;
  }

  // Add mentee to list of nodes.
  $mentee = correct_name($data[1]);
  if (!isset($nodes[$mentee])) {
    $nodes[$mentee] = $mentee;
  }

  foreach (parse_names($data[2]) as $name) {
    // Add mentors to list of nodes.
    $name = correct_name($name);
    if (!isset($nodes[$name])) {
      $nodes[$name] = $name;
    }

    // Calculate edges.
    $edge = new stdClass;
    $edge->source = $name;
    $edge->target = $mentee;
    $edge->value = MENTOR_SCORE;
    $edges[] = $edge;
  }
}

fclose($handle);

// Print out in GML format.
echo '<pre>';
$gml = 'graph
[';

foreach ($nodes as $name) {
  $gml .= "
  node
  [
    id $name
    label $name
  ]";
}

foreach ($edges as $edge) {
  $gml .= "
  edge
  [
    source $edge->source
    target $edge->target
    value $edge->value
  ]";
}

$gml .= "\n]";
echo $gml;
echo '</pre>';


/**
 * Make sense out of Profile module's "it could be commas or newlines" madness.
 */
function parse_names($data) {
  // Normalize commas and newlines.
  $data = str_replace(', ', ',', $data);
  $data = str_replace("\r\n", ',', $data);

  // Split the data on commas.
  $names = explode(',', $data);

  return $names;
}

/**
 * Correct data entry errors.
 */
function correct_name($name) {
  // Get rid of any extra space.
  $name = trim($name);

  // Make everything lowercase; makes it much easier to resolve differences.
  $name = strtolower($name);

  // Remove spaces; GML chokes on it.
  $name = str_replace(' ', '', $name);

  // Transliterate special characters; it also chokes on that.
  $name = str_replace('á', 'a', $name);

  // Correct misspellings/ambiguity in source data.
  $name = str_replace('jhodgson', 'jhodgdon', $name);
  $name = str_replace('killes@www.drop.org', 'killes', $name);
  $name = str_replace('merlinofchoas', 'merlinofchaos', $name);
  $name = str_replace('mortondk', 'mortendk', $name);
  $name = str_replace('tmplunkett', 'tim.plunkett', $name);
  $name = str_replace('timplunkett', 'tim.plunkett', $name);

  return $name;
}

/**
 * Calculate a super-simplistic score based on number of mentions.
 */
function leaderboard_score(&$nodes, $names) {
  // Calculate score.
  foreach ($names as $name) {
    if (empty($name)) {
      continue;
    }
    if (!isset($nodes[$name])) {
      $nodes[$name] = 0;
    }
    $nodes[$name]++;
  }
}

/**
 * Display a "leaderboard" of names + scores with increasing font sizes.
 */
function leaderboard_display($nodes) {
  // Put names in alphabetical order
  arsort($nodes);

  // Print the leaderboard.
  foreach ($nodes as $name => $score) {
    $size = $score + 12;
    print "<span style='font-size: $size'>$name ($score)</span><br />";
  }
}
