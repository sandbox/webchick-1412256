<?php

$csv = file_get_contents('./commits.csv');
$lines = explode("\r", $csv);
$new_lines = array();

foreach ($lines as $line) {
  list($contributor, $commits) = explode(',', $line);

  // Try and smooth out the superstars so more names fit on the cloud.
  if ($commits > 200) {
    $commits /= 2;
  }
  for ($i = 0; $i < $commits; $i++) {
    $contributor = trim($contributor);
    $contributor = str_replace(' ', '', $contributor);
    $contributor = str_replace('_', '', $contributor);
    $new_lines[] = "$contributor\n";
  }
}

file_put_contents('./processed.txt', $new_lines);

