Step 1: Go to http://ericduran.github.io/drupalcores/.

Step 2: Copy and paste the contents of the table into an Excel Spreadsheet. (Too big for Google :\)

Step 2b: Delete columns A and D (the overall rank + percentage), leaving just name + # of commits.

Step 3: File > Save as > CSV. Save to "commits.csv" in this folder.

Step 4: Run the `php tagify.php`

Step 5: Copy/paste the innards of processed.txt into Wordle.net.

Step 6: Play around with the settings until something looks cool. :D
